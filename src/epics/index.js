import { combineEpics } from 'redux-observable';
import pollRates from './pollRates';
import moneyTransfer from './moneyTransfer';

export default combineEpics(
  pollRates,
  moneyTransfer
);

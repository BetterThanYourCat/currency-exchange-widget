import { Observable } from 'rxjs';
import 'rxjs/add/observable/dom/ajax';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/delay';
import { 
  MONEY_TRANSFER, 
  moneyTransferSuccess
} from '../actions';

export default function moneyTransfer(action$, store) {
  return action$.ofType(MONEY_TRANSFER)
    .delay(100)
    .switchMap(() => {
      const { currencyToSend, currencyToReceive, amountToSend, amountToReceive } = store.value.exchange;
      const accountToSend = store.value.userAccounts.find((account) => account.currencyCode === currencyToSend);
      const accountToReceive = store.value.userAccounts.find((account) => account.currencyCode === currencyToReceive);
      
      const updatedAccountToSend = {
        ...accountToSend,
        balance: accountToSend.balance - amountToSend
      };

      const updatedAccountToReceive = {
        ...accountToReceive,
        balance: accountToReceive.balance + amountToReceive
      };

      return Observable.of(moneyTransferSuccess({
        [updatedAccountToSend.currencyCode]: updatedAccountToSend,
        [updatedAccountToReceive.currencyCode]: updatedAccountToReceive
      }));
    });
}

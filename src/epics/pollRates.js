import { Observable } from 'rxjs';
import 'rxjs/add/observable/dom/ajax';
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/map';

import { 
  INIT_EXCHANGE_WIDGET,
  SENDING_ACCOUNT_CHANGE, 
  exchangeRatesReceiveSuccess, 
  sendingAmountChange,
  receivingAmountChange
} from '../actions';

function getRateUrl(baseCurrency) {
  return `http://www.floatrates.com/daily/${baseCurrency}.json`;
}

export default function pollRates(action$, store) {
  return action$.ofType(SENDING_ACCOUNT_CHANGE, INIT_EXCHANGE_WIDGET)
    .switchMap((action) =>
      Observable.timer(0, 10000)
        .takeUntil(action$.ofType(SENDING_ACCOUNT_CHANGE))
        .exhaustMap(() => {
          const currencyToSend = action.type === SENDING_ACCOUNT_CHANGE
            ? action.payload.account.currencyCode
            : action.payload.currencyToSend;

          return Observable.ajax({ 
            url: getRateUrl(currencyToSend), 
            crossDomain: true
          })
          .mergeMap(res => {
            const rates = Object.keys(res.response)
              .reduce((rates, currencyCode) => {
                rates[currencyCode.toUpperCase()] = res.response[currencyCode].rate;
                return rates;
              }, {});

            rates[store.value.exchange.currencyToSend] = 1;

            return [
              exchangeRatesReceiveSuccess(rates),
              store.value.exchange.isSendingAmountLastEdited
                ? sendingAmountChange(store.value.exchange.amountToSend)
                : receivingAmountChange(store.value.exchange.amountToReceive)
            ];
          })
        })
    );
}

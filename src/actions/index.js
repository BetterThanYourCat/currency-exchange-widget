export const INIT_EXCHANGE_WIDGET = 'INIT_EXCHANGE_WIDGET';

export const SENDING_AMOUNT_CHANGE = 'SENDING_AMOUNT_CHANGE';
export const RECEIVING_AMOUNT_CHANGE = 'RECEIVING_AMOUNT_CHANGE';

export const SENDING_ACCOUNT_CHANGE = 'SENDING_ACCOUNT_CHANGE';
export const RECEIVING_ACCOUNT_CHANGE = 'RECEIVING_ACCOUNT_CHANGE';

export const EXCHANGE_RATES_RECEIVE_SUCCESS = 'EXCHANGE_RATES_RECEIVE_SUCCESS';

export const MONEY_TRANSFER = 'MONEY_TRANSFER';
export const MONEY_TRANSFER_SUCCESS = 'MONEY_TRANSFER_SUCCESS'


export function initExchangeWidget(currencyToSend, currencyToReceive) {
  return {
    type: INIT_EXCHANGE_WIDGET,
    payload: {
      currencyToSend,
      currencyToReceive
    }
  };
}

export function sendingAmountChange(amount) {
  return {
    type: SENDING_AMOUNT_CHANGE,
    payload: {
      amount
    }
  };
}

export function receivingAmountChange(amount) {
  return {
    type: RECEIVING_AMOUNT_CHANGE,
    payload: {
      amount
    }
  };
}

export function sendingAccountChange(account) {
  return {
    type: SENDING_ACCOUNT_CHANGE,
    payload: {
      account
    }
  };
}

export function receivingAccountChange(account) {
  return {
    type: RECEIVING_ACCOUNT_CHANGE,
    payload: {
      account
    }
  };
}

export function exchangeRatesReceiveSuccess(rates) {
  return {
    type: EXCHANGE_RATES_RECEIVE_SUCCESS,
    payload: {
      rates
    }
  };
}

export function moneyTransfer() {
  return {
    type: MONEY_TRANSFER
  };
}

export function moneyTransferSuccess(updatedAccounts) {
  return {
    type: MONEY_TRANSFER_SUCCESS,
    payload: updatedAccounts
  };
}
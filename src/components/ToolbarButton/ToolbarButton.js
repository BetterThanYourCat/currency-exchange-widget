import React from 'react';
import PropTypes from 'prop-types';
import styles from './ToolbarButton.module.css';

const ToolbarButton = ({disabled, onClick, children}) => (
  <button 
    disabled={disabled}
    onClick={onClick}
    className={styles.toolbarButton}>
    {children}
  </button>
);

ToolbarButton.propTypes = {
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.node,
};

export default ToolbarButton;
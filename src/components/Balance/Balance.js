import { formatCurrency } from '../../utils/currencyUtils';

const Balance = ({account}) => `You have ${formatCurrency(account.balance, account.currencyCode)}`;

export default Balance;
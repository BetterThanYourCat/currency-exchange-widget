import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import styles from './CurrencyField.module.css';

const CurrencyField = ({currencyCode, amount, onAmountChange, autoFocus, prefix}) => {
  let input;

  return (
    <div
      onClick={() => input.focus()}
      className={styles.currencyField}>

      <div
        className={styles.currencyCode}>
        {currencyCode}
      </div>

      <NumberFormat
        allowNegative={false}
        prefix={prefix}
        autoFocus={autoFocus}
        value={amount}
        decimalScale={2}
        getInputRef={(element) => input = element}
        onValueChange={(e) => onAmountChange(Math.abs(e.floatValue))} // When prefix is '-' NumberFormat emits event with negative number
        className={styles.currencyInput}/>
    </div>
  );
}

CurrencyField.propTypes = {
  currencyCode: PropTypes.string.isRequired,
  amount: PropTypes.number,
  onAmountChange: PropTypes.func,
  autoFocus: PropTypes.bool,
};

export default CurrencyField;
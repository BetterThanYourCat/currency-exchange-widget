import React from 'react';
import { formatCurrency } from '../../utils/currencyUtils';

const Rate = ({rate, currencyToSend, currencyToReceive}) => (
  <div>
    {formatCurrency(1, currencyToReceive)} = {formatCurrency(1 / rate, currencyToSend)}
  </div>
);

export default Rate;
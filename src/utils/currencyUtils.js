export function formatCurrency(amount, currencyCode) {
  return amount.toLocaleString('en', {
    currency: currencyCode,
    style: 'currency',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  })
}

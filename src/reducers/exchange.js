import {
  SENDING_AMOUNT_CHANGE, 
  RECEIVING_AMOUNT_CHANGE, 
  RECEIVING_ACCOUNT_CHANGE,
  SENDING_ACCOUNT_CHANGE,
  EXCHANGE_RATES_RECEIVE_SUCCESS,
  MONEY_TRANSFER,
  MONEY_TRANSFER_SUCCESS,
  INIT_EXCHANGE_WIDGET
} from "../actions";

const initialState = {
  currencyToSend: null,
  currencyToReceive: null,
  amountToSend: null,
  amountToReceive: null,
  isSendingAmountLastEdited: true,
  isTransferingMoney: false,
  rates: {}
}

export default function exchange(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case INIT_EXCHANGE_WIDGET:
      const { currencyToSend, currencyToReceive } = payload;
      return {
        ...state,
        currencyToSend,
        currencyToReceive
      };
    case SENDING_AMOUNT_CHANGE: {
      const rate = state.rates[state.currencyToReceive];
      return {
        ...state,
        amountToSend: payload.amount,
        amountToReceive: rate && payload.amount && (rate * payload.amount),
        isSendingAmountLastEdited: true
      };
    }
    case RECEIVING_AMOUNT_CHANGE: {
      const rate = state.rates[state.currencyToReceive];
      return {
        ...state,
        amountToReceive: payload.amount,
        amountToSend: rate && (payload.amount / rate),
        isSendingAmountLastEdited: false
      };
    }
    case SENDING_ACCOUNT_CHANGE:
      return {
        ...state,
        currencyToSend: payload.account.currencyCode,
        amountToReceive: null,
        rates: null,
      };
    case RECEIVING_ACCOUNT_CHANGE:
      const rate = state.rates[payload.account.currencyCode];
      let amountToReceive;
      
      if (state.amountToSend && rate) { 
        amountToReceive = rate * state.amountToSend;
      } 
      return {
        ...state,
        amountToReceive,
        currencyToReceive: payload.account.currencyCode,
      };
    case EXCHANGE_RATES_RECEIVE_SUCCESS:
      const { rates } = payload;
      return {
        ...state,
        rates,
      };
    case MONEY_TRANSFER:
      return {
        ...state,
        isTransferingMoney: true,
      };
    case MONEY_TRANSFER_SUCCESS:
      return {
        ...state,
        amountToSend: null,
        amountToReceive: null,
        isTransferingMoney: false
      };
    default:
      return state;
  }
}

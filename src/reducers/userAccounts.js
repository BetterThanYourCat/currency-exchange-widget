import { MONEY_TRANSFER_SUCCESS } from '../actions/index';

const initialState = [
  {
    currencyCode: 'USD',
    balance: 25.51
  },
  {
    currencyCode: 'EUR',
    balance: 116.12
  },
  {
    currencyCode: 'GBP',
    balance: 58.33
  }
];

export default function userAccounts(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case MONEY_TRANSFER_SUCCESS:
      return state.map((account) => {
        if (payload[account.currencyCode]) {
          return payload[account.currencyCode];
        }

        return account;
      });
    default:
      return state;
  }
}

import { combineReducers } from 'redux';
import userAccounts from './userAccounts';
import exchange from './exchange';

export default combineReducers({
  userAccounts,
  exchange
});

import React from 'react'
import { connect } from 'react-redux';
import {
  sendingAmountChange, 
  receivingAmountChange, 
  sendingAccountChange, 
  receivingAccountChange, 
  moneyTransfer 
} from '../../actions';
import Balance from '../../components/Balance';
import CurrencyField from '../../components/CurrencyField/';
import ToolbarButton from '../../components/ToolbarButton/ToolbarButton';
import styles from './ExchangeWidget.module.css';
import {  } from '../../actions/index';
import Rate from '../../components/Rate';

class ExchangeWidget extends React.Component {
  handleTransfer = () => {
    this.props.moneyTransfer();
  }

  handleAccountToSendChange = (direction) => {
    this.props.sendingAccountChange(
      this.getNextAccount(this.props.accountToSend, direction)
    );
  }

  handleAccountToReceiveChange = (direction) => {
    this.props.receivingAccountChange(
      this.getNextAccount(this.props.accountToReceive, direction)
    );
  }

  getNextAccount = (account, direction) => {
    const { userAccounts } = this.props;
    const accountIndex = userAccounts.indexOf(account);

    let nextAccountIndex;
    
    if (direction === 'right') {
      nextAccountIndex = (accountIndex + 1) % userAccounts.length;
    } else {
      nextAccountIndex = accountIndex - 1;

      if (nextAccountIndex < 0) {
        nextAccountIndex = userAccounts.length - 1;
      }
    }

    return userAccounts[nextAccountIndex];
  }

  render = () => {
    const { 
      accountToSend, 
      accountToReceive, 
      amountToSend, 
      amountToReceive,
      rate,
      canTransfer
    } = this.props;

    if (!accountToSend || !accountToReceive) {
      return <div>Loading...</div>;
    }

    return (
      <div
        className={styles.exchangeWidget}>

        <div
          className={styles.topContainer}>

          <div
            className={styles.toolbar}>

            <ToolbarButton
              disabled={!canTransfer}
              onClick={this.handleTransfer}
              className={styles.toolbarButton}>
              Exchange
            </ToolbarButton>              
          </div>

          <div
            className={styles.fieldContainer}>

            <div 
              onClick={() => this.handleAccountToSendChange('left')}
              className={styles.arrowLeft}>
            </div>  

            <CurrencyField
              autoFocus={true}
              amount={amountToSend}
              currencyCode={accountToSend.currencyCode}
              prefix={'-'}
              onAmountChange={(value) => this.props.sendingAmountChange(value)} />

            <Balance account={accountToSend}/>

            <div 
              onClick={() => this.handleAccountToSendChange('right')}
              className={styles.arrowRight}>
            </div>  
          </div>
        </div>

        <div
          className={styles.bottomContainer}>

          <div
            className={styles.fieldContainer}>

            <div 
              onClick={() => this.handleAccountToReceiveChange('left')}
              className={styles.arrowLeft}>
            </div>  

            <CurrencyField
              amount={amountToReceive}
              currencyCode={accountToReceive.currencyCode}
              prefix={'+'}
              onAmountChange={(value) => this.props.receivingAmountChange(value)} />

            <div
              className={styles.exchangeDetails}>

              <Balance account={accountToReceive}/>

              {rate && 
                <Rate
                  rate={rate}
                  currencyToSend={accountToSend.currencyCode}
                  currencyToReceive={accountToReceive.currencyCode}/>}
            </div>

            <div 
              onClick={() => this.handleAccountToReceiveChange('right')}
              className={styles.arrowRight}>
            </div>  
          </div>
        </div>
      </div>
    );
  };
}

const rateSelector = ({currencyToReceive, rates}) => !currencyToReceive || !rates
  ? null
  : rates[currencyToReceive];

const mapStateToProps = ({exchange, userAccounts}) => {
  const accountToSend = userAccounts.find((account) => account.currencyCode === exchange.currencyToSend);
  const accountToReceive = userAccounts.find((account) => account.currencyCode === exchange.currencyToReceive);
  const canTransfer = accountToSend != null && accountToReceive != null && !exchange.isTransferingMoney &&
    (accountToSend.balance >= exchange.amountToSend && exchange.amountToSend > 0) && 
    (accountToSend !== accountToReceive);

  const rate = rateSelector(exchange);

  return {
    accountToSend,
    accountToReceive,
    canTransfer,
    userAccounts,
    rate,
    ...exchange,
  }
};

const mapDispatchToProps = {
  sendingAmountChange,
  receivingAmountChange,
  sendingAccountChange,
  receivingAccountChange,
  moneyTransfer
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExchangeWidget);

import React, { Component } from 'react';
import ExchangeWidget from '../ExchangeWidget/ExchangeWidget';
import styles from './App.module.css';
import { connect } from 'react-redux';
import { initExchangeWidget } from '../../actions/index';

class App extends Component {
  componentDidMount = () => {
    this.props.initExchangeWidget('USD', 'GBP');
  }

  render() {
    return (
      <div className={styles.wrapper}>
        <div className={styles.pageContainer}>
          <ExchangeWidget />
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  {initExchangeWidget}
)(App);
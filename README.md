# Currency exchange widget

Very simple currency exchange widget

## Usage

Run command: ```npm i && npm start```


## Restrictions

Due to https://openexchangerates.org API limitation it's not possible to fetch rates of any base currency except `USD`
